package advisory

import "fmt"

// ErrNoAdvisoryForPackageType is the error returned when there are no advisories
// corresponding to a given package type.
type ErrNoAdvisoryForPackageType struct {
	PackageType string
}

func (err ErrNoAdvisoryForPackageType) Error() string {
	return fmt.Sprintf("no advisory for package type %s", err.PackageType)
}

// ErrNoPackageTypeDir is the error returned when the directory corresponding
// to a package type is missing, or cannot be read.
type ErrNoPackageTypeDir struct {
	PackageType string
}

func (err ErrNoPackageTypeDir) Error() string {
	return fmt.Sprintf("cannot read directory for package type %s", err.PackageType)
}

// ErrNoPackageDir is the error returned when the directory containing
// package advisories is missing, or cannot be read.
type ErrNoPackageDir struct {
	Package Package
}

func (err ErrNoPackageDir) Error() string {
	return fmt.Sprintf("cannot read directory for package %s", err.Package.Slug())
}

// ErrAmbiguousPackageDir is the error returned when there is more than
// one directory that matches a queried package.
type ErrAmbiguousPackageDir struct {
	Package Package
}

func (err ErrAmbiguousPackageDir) Error() string {
	return fmt.Sprintf("ambiguous result when trying to match package %s", err.Package.Slug())
}
