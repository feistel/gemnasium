package pip

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/builder/pipdeptree"
)

const (
	flagPipVersion     = "pip-version"
	flagDependencyPath = "pip-dependency-path"
	flagPipCertPath    = "pip-cert-path"
	flagGetPipPath     = "get-pip-path"

	pathPip            = "pip"
	pathPython         = "python"
	pathPipdeptreeJSON = "pipdeptree.json"
)

// Builder generates dependency lists for pip projects
type Builder struct {
	CacheDir  string // CacheDir is where the Python packages are already installed
	SkipFetch bool   // SkipFetch is set when the Python packages are already installed
}

// Flags returns the CLI flags that configure the pip command
func (b Builder) Flags() []cli.Flag {
	return []cli.Flag{
		&cli.StringFlag{
			Name:    flagPipVersion,
			Usage:   "Pip version to install and use",
			EnvVars: []string{"DS_PIP_VERSION"},
		},
		&cli.StringFlag{
			Name:    flagDependencyPath,
			Usage:   "Path to directory with pip dependencies",
			EnvVars: []string{"DS_PIP_DEPENDENCY_PATH"},
		},
		&cli.StringFlag{
			Name:    flagPipCertPath,
			Usage:   "Path to certificate file to be used in pip and pipenv installs",
			EnvVars: []string{"PIP_CERT"},
		},
		&cli.StringFlag{
			Name:    flagGetPipPath,
			Usage:   "Path to the get-pip.py script",
			EnvVars: []string{"DS_GET_PIP_PATH"},
			Value:   "get-pip.py",
		},
	}
}

// Configure configures the pip command
func (b *Builder) Configure(c *cli.Context) error {
	// install requested version of pip
	if version := c.String(flagPipVersion); version != "" {
		installPip(c.String(flagGetPipPath), version)
	}

	// set PIP_CERT if not set and CA cert bundle is set
	if !c.IsSet(flagPipCertPath) && c.IsSet(cacert.FlagBundleContent) {
		if err := os.Setenv("PIP_CERT", cacert.DefaultBundlePath); err != nil {
			return err
		}
	}

	// if dependency path is passed explicitly, skip fetching and rely on cache
	if c.String(flagDependencyPath) != "" {
		b.CacheDir = c.String(flagDependencyPath)
		b.SkipFetch = true
	} else {
		b.CacheDir = "./dist"
		b.SkipFetch = false
	}

	return nil
}

func installPip(getPipPath, version string) error {
	versionString := fmt.Sprintf(`pip==%s`, version)
	cmd := exec.Command(pathPython, getPipPath, "--disable-pip-version-check", "--no-cache-dir", versionString)
	cmd.Dir = "/"
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

// Build generates a dependency list for a pip requirements file, and returns its path
func (b Builder) Build(input string) (string, error) {
	// install dependencies using setuptools script
	if err := b.installDeps(input); err != nil {
		return "", err
	}

	// save JSON output of pipdeptree
	output := filepath.Join(filepath.Dir(input), pathPipdeptreeJSON)
	return output, pipdeptree.CreateJSON(output)
}

func (b Builder) installDeps(input string) error {
	dir, file := filepath.Split(input)

	// fetch dependencies unless dependencyPath is already set
	if !b.SkipFetch {
		cmd := exec.Command(pathPip, "download", "--disable-pip-version-check", "--dest", b.CacheDir, "-r", file)
		cmd.Dir = dir
		cmd.Env = os.Environ()
		output, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), output)
		if err != nil {
			return err
		}
	}

	// arguments when calling pip
	args := []string{
		"install",
		"--disable-pip-version-check",
		"--find-links", b.CacheDir,
		"--requirement", file,
	}

	// when dependencies are passed explicitly, rely entirely off `find-links` path
	if b.SkipFetch {
		args = append(args, "--no-index")
	}

	// run pip install
	cmd := exec.Command(pathPip, args...)
	cmd.Dir = dir
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

func init() {
	builder.Register("pip", &Builder{})
}
