package golang

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange"
)

func TestResolve(t *testing.T) {
	// test cases
	var tcs = []struct {
		name      string       // test case name
		query     vrange.Query // input query
		satisfies bool         // expected to match?
		fails     bool         // expected to fail?
	}{
		{
			name: "semantic range, stable version",
			query: vrange.Query{
				Range:   "<v1.2.3",
				Version: "v1.2.3",
			},
			satisfies: false,
		},
		{
			name: "semantic range (disjunction with conjunction), stable version",
			query: vrange.Query{
				Range:   "<=v1.2.3 || =v5.6.4 || >6.0.0 <v6.5.3",
				Version: "v5.6.4",
			},
			satisfies: true,
		},
		{
			name: "semantic range (disjunction with conjunction), stable version, excluded",
			query: vrange.Query{
				Range:   "<=v1.2.3 || =v5.6.4 || >6.0.0 <v6.5.3",
				Version: "v5.6.5",
			},
			satisfies: false,
		},
		{
			name: "empty range, stable version",
			query: vrange.Query{
				Range:   "",
				Version: "v5.6.5",
			},
			fails: true,
		},
		{
			name: "pseudo-version range, stable version",
			query: vrange.Query{
				Range:   ">=v0.0.0-20181107220215-6f38f9bf7126",
				Version: "1.1",
			},
			fails: true,
		},
		{
			name: "pseudo-version range, pseudo-version",
			query: vrange.Query{
				Range:   "<v0.0.0-20191112214715-2bda9f819ef6",
				Version: "v0.0.0-20131112214715-2bda9f819ef6",
			},
			satisfies: true,
		},
		{
			name: "mixed range (disjunction), stable version",
			query: vrange.Query{
				Range:   "<=v1.1.4||<=v0.0.0-20190227021215-0ee087269130",
				Version: "1.1.1",
			},
			fails: true,
		},
		{
			name: "pseudo-version range (disjunction), invalid pseudo-version",
			query: vrange.Query{
				Range:   "<v0.0.0-20190806021115-e847f0a6603d||>=v0.0.0-20190618212315-adea3b3cbe29",
				Version: "v0.0.0-20190806021115-aaaa",
			},
			fails: true,
		},
		{
			name: "pseudo-version range (conjunction), pseudo-version",
			query: vrange.Query{
				Range:   ">=v0.0.0-20190911225915-59d7fe166833 <v0.0.0-20191107144515-ba23e655e1fa",
				Version: "v0.0.0-20201112214715-2bda9f819ef6",
			},
			satisfies: false,
		},
		{
			name: "pseudo-version range, pseudo-version",
			query: vrange.Query{
				Range:   "<v0.0.0-20190627181215-274ead3ed21b",
				Version: "v0.0.0.1",
			},
			fails: true,
		},
		{
			name: "semantic range (conjunction), stable version",
			query: vrange.Query{
				Range:   ">v1.6.2 <=v1.6.2",
				Version: "1.2.3",
			},
			satisfies: false,
		},
		{
			name: "pseudo-version range (disjunction with conjunction), pseudo-version",
			query: vrange.Query{
				Range:   ">=v0.0.0-20190123215315-55422c2b835d <=v0.0.0-20191113222515-4a1b228a25c0||>=v0.0.0-20201112214715-2bda9f819ef6",
				Version: "v0.0.0-20201112214715-2bda9f819ef6",
			},
			satisfies: true,
		},
		{
			name: "pseudo-version range (conjunction), pseudo-version",
			query: vrange.Query{
				Range:   ">=v0.0.0-20190123215315-55422c2b835d <=v0.0.0-20191113222515-4a1b228a25c0",
				Version: "v0.0.0-20191113222515-55422c2b835d",
			},
			satisfies: true,
		},
		{
			name: "semantic range (equality), stable version",
			query: vrange.Query{
				Range:   "=v1.4.3",
				Version: "1.4.3",
			},
			satisfies: true,
		},
		{
			name: "mixed range (conjunction), stable version",
			query: vrange.Query{
				Range:   "<v1.8.0||<v0.0.0-20190420192315-d4da1e304c3c",
				Version: "v1.8.0",
			},
			fails: true,
		},
		{
			name: "semantic range (conjunction), stable version",
			query: vrange.Query{
				Range:   ">=v1.7.2 <=v1.7.3",
				Version: "1.7.2",
			},
			satisfies: true,
		},
		{
			name: "semantic range (pre-release), pre-release version",
			query: vrange.Query{
				Range:   "<=v1.0.0-rc8",
				Version: "v1.0.0-rc7",
			},
			satisfies: true,
		},
		{
			name: "pseudo-version range (conjunction), pseudo-version matching upper bound",
			query: vrange.Query{
				Range:   ">=v0.0.0-20180501080515-14c6b3e8f903 <=v0.0.0-20190104020215-27809385301f",
				Version: "v0.0.0-20190104020215-27809385301f",
			},
			satisfies: true,
		},
		{
			name: "pseudo-version range (conjunction), pseudo-version matching lower bound",
			query: vrange.Query{
				Range:   ">=v0.0.0-20180501080515-14c6b3e8f903 <=v0.0.0-20190511060715-b1896822e58b",
				Version: "v0.0.0-20180501080515-14c6b3e8f903",
			},
			satisfies: true,
		},
		{
			name: "pseudo-version range (equality), pseudo-version",
			query: vrange.Query{
				Range:   "==v0.0.0-20181217181515-444497330ac7",
				Version: "v0.0.0-20181217181515-444497330ac7",
			},
			satisfies: true,
		},
		{
			name: "semantic range (disjunction with conjunction), stable version",
			query: vrange.Query{
				Range:   ">=1.8.0<=1.8.3||=1.9.0",
				Version: "v1.9.0",
			},
			satisfies: true,
		},
		{
			name: "semantic range (disjunction of conjunctions), stable version",
			query: vrange.Query{
				Range:   ">=1.7.0 <=1.7.5||>=1.8.0 <=1.8.2||>1.8.3 <1.9.0",
				Version: "v1.8.1",
			},
			satisfies: true,
		},
		{
			name: "semantic range (alpha pre-release), beta.1 pre-release",
			query: vrange.Query{
				Range:   ">=1.7.8-alpha",
				Version: "1.7.8-beta.1",
			},
			satisfies: true,
		},
		{
			name: "semantic range (pre-release version), ALPHA pre-release",
			query: vrange.Query{
				Range:   ">=1.7.8-0",
				Version: "1.7.8-ALPHA",
			},
			satisfies: true,
		},
		{
			name: "semantic range (stable version), alpha pre-release",
			query: vrange.Query{
				Range:   "<1.13.0",
				Version: "1.13.0-alpha",
			},
			satisfies: true,
		},
		{
			name: "semantic range (conjunction of disjunctions), beta pre-release",
			query: vrange.Query{
				Range:   ">=1.11.0 <1.11.9||>=1.12.0 <1.12.7||>=1.13.0 <1.13.5||=1.14.0",
				Version: "v1.12.7-beta",
			},
			satisfies: true,
		},
	}

	// collect queries
	var queries []vrange.Query
	for _, tc := range tcs {
		queries = append(queries, tc.query)
	}

	// resolve queries
	resolver := Resolver{}
	results, err := resolver.Resolve(queries)
	require.NoError(t, err)

	// check results
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			satisfies, err := results.Satisfies(tc.query)
			if err != nil {
				require.Truef(t, tc.fails, "query '%s' failed: %s", tc.query.String(), err)
				return // failed as expected
			}
			require.Falsef(t, tc.fails, "expected query '%s' to fail, got no error", tc.query.String())
			require.Equalf(t, tc.satisfies, satisfies, "wrong result for query '%s'. expected %v, got %v", tc.query.String(), tc.satisfies, satisfies)
		})
	}
}

func TestTranslateQuery(t *testing.T) {
	// test cases
	var tcs = []struct {
		name     string             // test case name
		query    vrange.Query       // input query
		versions []advisory.Version // versions metadata
		want     vrange.Query       // expected translated query
	}{
		{
			name: "unchanged",
			query: vrange.Query{
				Range:   ">v1.2.1 <v3.5.4",
				Version: "v2.4.2",
			},
			want: vrange.Query{
				Range:   ">v1.2.1 <v3.5.4",
				Version: "v2.4.2",
			},
		},
		{
			name: "pseudo",
			query: vrange.Query{
				Range:   "<v2019.03.04.00",
				Version: "v0.0.0-20170304035542-fc82aa6cfdb4",
			},
			versions: []advisory.Version{
				{
					Number: "v2019.03.04.00",
					Commit: advisory.Commit{
						Tags: []string{
							"v2019.03.04.00",
						},
						Sha:       "fc82aa6cfdb437dabf18c606ed23129e5973b5ea",
						Timestamp: "20190304035542",
					},
				},
			},
			want: vrange.Query{
				Range:   "<v0.0.0-20190304035542-fc82aa6cfdb4",
				Version: "v0.0.0-20170304035542-fc82aa6cfdb4",
			},
		},
		{
			name: "semantic",
			query: vrange.Query{
				Range:   ">v1.2.1 <v3.5.4 || <v0.0.0-20170304035542-fc82aa6cfdb4",
				Version: "v2.4.2",
			},
			want: vrange.Query{
				Range:   ">v1.2.1 <v3.5.4",
				Version: "v2.4.2",
			},
		},
	}

	resolver := Resolver{}
	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			got := resolver.TranslateQuery(tc.query, tc.versions)
			require.Equalf(t, tc.want.Version, got.Version, "wrong version. expected %s, got %s", tc.want.Version, got.Version)
			require.Equalf(t, tc.want.Range, got.Range, "wrong range. expected %s, got %s", tc.want.Range, got.Range)
		})
	}
}
