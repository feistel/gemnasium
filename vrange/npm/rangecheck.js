#!/usr/bin/env node

let documents;

const bail = (error) => {
  console.log(error.message)
  process.exit(1);
}

var path = require('path');

try {
  const fs = require('fs');

  if(!process.argv[2]){
    base = path.basename(process.argv[1])
    throw new Error("Usage: ./" + base + " <JSON input file>")
  }

  documents = JSON.parse(fs.readFileSync(process.argv[2], 'utf8'));

  if(!Array.isArray(documents)){
    throw new Error('File input is not a JSON array.')
  }

} catch(e){
  console.error(e.message);
  process.exit(1);
}

const {processDocument} = require('./shared');

console.log(JSON.stringify(documents.map(processDocument),null,2))
