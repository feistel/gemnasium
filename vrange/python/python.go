package python

import "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/vrange/cli"

func init() {
	cli.Register("python", "python/rangecheck.py")
}
