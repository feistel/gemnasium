package poetry

import (
	"io"

	"github.com/BurntSushi/toml"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

type docPackage struct {
	Name    string
	Version string
}
type docMetadata struct {
	ContentHash    string `toml:"content-hash"`
	PythonVersions string `toml:"python-versions"`
}

type document struct {
	Packages []docPackage `toml:"package"`
	Metadata docMetadata
}

// Parse scans a Poetry lock file and returns a list of packages
func Parse(r io.Reader) ([]parser.Package, []parser.Dependency, error) {
	doc := document{}
	_, err := toml.DecodeReader(r, &doc)
	if err != nil {
		return nil, nil, err
	}
	if doc.Metadata.PythonVersions == "" || doc.Metadata.ContentHash == "" {
		return nil, nil, parser.ErrWrongFileFormatVersion
	}
	result := make([]parser.Package, len(doc.Packages))
	for i, pkg := range doc.Packages {
		result[i] = parser.Package{Name: pkg.Name, Version: pkg.Version}
	}
	return result, nil, nil
}

func init() {
	parser.Register("poetry", parser.Parser{
		Parse:          Parse,
		PackageManager: "poetry",
		PackageType:    parser.PackageTypePypi,
		Filenames:      []string{"poetry.lock"},
	})
}
