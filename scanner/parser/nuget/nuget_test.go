package nuget

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

func TestNuget(t *testing.T) {
	t.Run("Parse", func(t *testing.T) {
		t.Run("wrong version", func(t *testing.T) {
			// Load fixture
			fixture, err := os.Open("fixtures/wrong_version/packages.lock.json")
			require.NoError(t, err, "Can't open fixture file")
			defer fixture.Close()
			_, _, err = Parse(fixture)
			require.EqualError(t, err, parser.ErrWrongFileFormatVersion.Error())
		})

		for _, tc := range []string{"web.api", "duplicates"} {
			t.Run(tc, func(t *testing.T) {
				// Load fixture
				fixture, err := os.Open("fixtures/" + tc + "/packages.lock.json")
				require.NoError(t, err, "Can't open fixture file")
				defer fixture.Close()
				pkgs, deps, err := Parse(fixture)
				require.NoError(t, err)

				t.Run("packages", func(t *testing.T) {
					// Load expected output
					expect, err := os.Open("expect/" + tc + "/packages.json")
					require.NoError(t, err, "Can't open expect file")
					defer expect.Close()
					var want []parser.Package
					err = json.NewDecoder(expect).Decode(&want)
					require.NoError(t, err)

					require.ElementsMatch(t, want, pkgs)
				})

				t.Run("dependencies", func(t *testing.T) {
					// Load expected output
					expect, err := os.Open("expect/" + tc + "/dependencies.json")
					require.NoError(t, err, "Can't open expect file")
					defer expect.Close()
					var want []parser.Dependency
					err = json.NewDecoder(expect).Decode(&want)
					require.NoError(t, err)

					require.ElementsMatch(t, want, deps)
				})
			})
		}
	})
}
