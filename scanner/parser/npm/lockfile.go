package npm

import (
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v2/scanner/parser"
)

// Lockfile is a npm lock file
type Lockfile struct {
	LockfileVersion int64              `json:"lockfileVersion"`
	Packages        map[string]Package `json:"packages"`
	Dependencies    Dependencies       `json:"dependencies"`
}

// Package is a package the project depends on
type Package struct {
	Version string `json:"version"`
}

// Dependency is a package the project depends on
type Dependency struct {
	// Version is the package version
	Version string `json:"version"`

	// Dependencies are transient dependencies
	// that are local to the package, as opposed to shared dependencies.
	Dependencies Dependencies `json:"dependencies"`
}

// Dependencies maps a dependency name to a dependency description
type Dependencies map[string]Dependency

// Parse returns packages without duplicates
func (f Lockfile) Parse() ([]parser.Package, error) {
	switch f.LockfileVersion {
	case 1:
		// parse "dependencies" field of lockfile v1
		return dedupePkgs(parseDeps(f.Dependencies)), nil
	case 2:
		// parse "packages" field of lockfile v2
		return dedupePkgs(f.parsePkgs()), nil
	default:
		// not supported
		return nil, parser.ErrWrongFileFormatVersion
	}
}

// dedupePkgs removes duplicate packages
func dedupePkgs(pkgs []parser.Package) []parser.Package {
	keys := make(map[string]bool)
	list := []parser.Package{}
	for _, pkg := range pkgs {
		key := pkg.Name + "@" + pkg.Version
		if _, found := keys[key]; !found {
			keys[key] = true
			list = append(list, pkg)
		}
	}
	return list
}

// parseDeps walks through the "dependencies" and convert them to packages.
// This is compatible with lockfile v1 only
// because in lockfile v2 the "version" field might be missing.
func parseDeps(deps Dependencies) []parser.Package {
	pkgs := []parser.Package{}
	for name, dep := range deps {
		// add dependency as a package
		pkgs = append(pkgs, parser.Package{Name: name, Version: dep.Version})
		// add its dependencies as packages
		pkgs = append(pkgs, parseDeps(dep.Dependencies)...)
	}
	return pkgs
}

// parsePkgs iterates objects of the "packages" field and convert them.
// This is compatible with lockfile v2 only because "packages" doesn't exist in lockfile v1.
func (f Lockfile) parsePkgs() []parser.Package {
	pkgs := []parser.Package{}
	for path, info := range f.Packages {
		// skip package that corresponds to the project itself
		if path == "" {
			continue
		}

		// the package name is what comes after last occurrence of "node_modules/"
		parts := strings.SplitAfter(path, "node_modules/")
		name := parts[len(parts)-1]
		pkgs = append(pkgs, parser.Package{Name: name, Version: info.Version})
	}
	return pkgs
}
